#FROM nginx/unit:1.23.0-php7.4 as base
#FROM piotrpazola/nginx-unit:1.20.0-php7.4-pdo-mysql
#FROM debian
FROM php:7.4


ENV APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE DontWarn
ENV DEBIAN_FRONTEND noninteractive

ENV PHP_VERSION         7.4
ENV UNIT_GIT_VERSION    1.20.0-1
ENV PHP_KAFKA_VERSION   4.0.3

RUN apt update \
    && apt install -y --no-install-recommends ca-certificates curl gnupg gnupg2 gnupg1\
    && curl -s --output /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg \
    && echo "deb https://packages.sury.org/php/ buster main" |  tee /etc/apt/sources.list.d/php.list

RUN apt update

RUN apt-get install -y -q --no-install-recommends \
		    git \
                    vim \
                    nano \
                    less \
                    mc \
                    wget \
                    zip \
                    unzip \
                    apt-utils \
                    procps \
                    gcc \
                    build-essential

RUN cd /opt \
    && wget -O unit.tar.gz https://github.com/nginx/unit/archive/${UNIT_GIT_VERSION}.tar.gz \
    && tar -C ./ -xvf unit.tar.gz --strip-components 1  \
    && ./configure --modules=modules \
    && ./configure php --module=php  \
    && DESTDIR=/usr/lib/unit/  make \
    && DESTDIR=/usr/lib/unit/ make install \
    && rm -Rf /opt/* 

RUN apt install -y -q --no-install-recommends \
                    libpq-dev \
                    libmcrypt-dev \
                    libxslt-dev \
                    libbz2-dev \
                    zlib1g-dev \
                    libpng-dev \
                    libc-client-dev \
                    libkrb5-dev \
                    libldb-dev libldap2-dev \
                    libsodium-dev \
                    libzip-dev \
                    libfreetype6 libfreetype6-dev \
                    libjpeg-dev \
                    zlib1g-dev \
                    libpng16-16 libpng-dev \
                    libgd3 libgd-dev \
		    libmemcached-dev \
		    libwebp-dev \
		    libxml2-dev \
		    libmagickwand-dev \
		    libtidy-dev

RUN rm -r /var/lib/apt/lists/* \
    && ln -s /usr/lib/x86_64-linux-gnu/libldap.so /usr/lib/libldap.so \
    && ln -s /usr/lib/x86_64-linux-gnu/liblber.so /usr/lib/liblber.so

RUN pecl install mcrypt-1.0.3

RUN docker-php-ext-configure imap --with-kerberos --with-imap-ssl
RUN docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp --with-xpm
RUN docker-php-ext-install pdo_mysql pdo_pgsql bcmath bz2 calendar exif gd gettext imap intl ldap mysqli opcache pcntl shmop soap sockets sodium sysvmsg sysvsem sysvshm xmlrpc xsl zip

ENV COMPOSER_ALLOW_SUPERUSER    1
ENV COMPOSER_VERSION            2.1.5

RUN curl -o /tmp/composer-setup.php https://getcomposer.org/installer \
    && php /tmp/composer-setup.php --no-ansi --version="${COMPOSER_VERSION}" --install-dir=/usr/local/bin --filename=composer \
    && composer -V

RUN composer global require phpunit/phpunit
ENV PATH="${PATH}:/app/vendor/bin"

RUN apt update && apt install -y iputils-ping telnet mc

#RUN mkdir /usr/lib/unit/state/
#RUN mkdir /usr/lib/unit/state/certs/

ENTRYPOINT ["/usr/local/bin/docker-unit.sh"]
CMD ["/usr/lib/unit/sbin/unitd", "--no-daemon", "--control", "unix:/var/run/control.unit.sock", "--pid", "/var/run/unit.pid", "--log", "/var/log/unit.log", "--modules", "/usr/lib/unit/modules", "--state", "/usr/lib/unit/state/", "--tmp", "/var/tmp/", "--user", "root"]
COPY ./docker-unit.sh /usr/local/bin/
COPY ./docker-entrypoint.sh ./docker-unit-config.json /docker-entrypoint.d/

RUN mkdir /app && echo "<?php phpinfo();" > /app/index.php

#RUN rm -rf /app/var/log/* && ln -s /dev/stdout /app/var/log/exception.log && ln -s /dev/stdout /app/var/log/system.log && ln -s /dev/stdout /app/var/log/debug.log
