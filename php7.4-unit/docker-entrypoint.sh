#!/bin/bash -e

# Directory structure preparation
# Time Setting
export TZ=${TZ:-"UTC"}
ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# PHP version
php -v
php -m

# Migrations
